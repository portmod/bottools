# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import argparse
import re
import sys
from typing import Dict, List, Optional, Union

import gitlab
from portmod.loader import load_all
from portmod.pybuild import Pybuild
from portmod.repo import get_repo
from portmod.repo.metadata import get_maintainer_groups, get_package_metadata
from portmodlib.atom import ver_re
from portmodlib.portmod import Group, PackageMetadata, Person

from update import get_project


def get_names() -> Dict[str, Pybuild]:
    return {mod.CPN: mod for mod in load_all()}


def get_mod(string: str, names: Dict[str, Pybuild]) -> Optional[Pybuild]:
    for name in names:
        # Make sure that the package name isn't suffixed/prefixed by anything
        if re.search(f"[^A-Za-z-_]{name}([^A-Za-z-_]|-{ver_re})", string):
            return names[name]

    return None


def assign_issues():
    parser = argparse.ArgumentParser(
        description="Interface to automatically assigning issues to \
        Portmod mod maintainers"
    )
    parser.add_argument(
        "--gitlab-project",
        help="The gitlab project group/name for submitting the issues",
    )
    args = parser.parse_args()

    if len(sys.argv) == 1:
        parser.print_help()
        return

    gl = gitlab.Gitlab.from_config()
    proj = get_project(*args.gitlab_project.split("/"))
    names = get_names()

    def get_maintainer_ids(
        pkg: Pybuild, metadata: PackageMetadata, maintainer: Union[Person, Group]
    ) -> List[int]:
        if isinstance(maintainer, Person):
            if maintainer.email:
                gl_maintainer = gl.users.list(search=maintainer.email)
            elif maintainer.name:
                gl_maintainer = gl.users.list(search=maintainer.name)
            else:
                raise Exception(f"Invalid maintainer {maintainer}")

            if len(gl_maintainer) > 1:
                raise Exception(
                    f"Maintainer {maintainer} for package {pkg} is ambiguous "
                    f"and could refer to any of the following users: {gl_maintainer}"
                )
            if gl_maintainer:
                return [gl_maintainer[0].id]
            print(
                f'WARNING: No maintainer for matching "{maintainer}" could be found for package {pkg}!'
            )
            return []
        elif isinstance(maintainer, Group):
            result = []
            for nested in get_maintainer_groups(get_repo(pkg.REPO).location)[
                maintainer.group
            ].members:
                result.extend(get_maintainer_ids(pkg, metadata, nested))
            return result
        raise Exception(f"Invalid maintainer {maintainer}")

    for issue in proj.issues.list(all=True, state="opened"):
        if not issue.assignees:
            pkg = get_mod(issue.title, names)
            if pkg:
                metadata = get_package_metadata(pkg)
                if metadata and metadata.maintainer:
                    print(metadata.maintainer)
                    issue.assignee_ids = []
                    if isinstance(metadata.maintainer, list):
                        print(type(metadata.maintainer))
                        for obj in metadata.maintainer:
                            issue.assignee_ids.extend(
                                get_maintainer_ids(pkg, metadata, obj)
                            )
                    else:
                        issue.assignee_ids.extend(
                            get_maintainer_ids(pkg, metadata, metadata.maintainer)
                        )
                    issue.save()


if __name__ == "__main__":
    assign_issues()
