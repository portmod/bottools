#!/bin/bash

MIRROR=$1
MESSAGE=$3
REPO=$2

if [[ -r $MIRROR ]]; then
	cd "$MIRROR" || exit 1
	git pull --quiet
	git lfs fetch
	echo portmod mirror $MIRROR
	portmod mirror $MIRROR $REPO
	for file in $(git ls-files --others --exclude-standard); do
		ext=${file##*.}
		if [[ $ext != "zip" && $ext != "7z" && $ext != "rar" && $ext != "gz" && $ext != "xz" && $ext != "bz2"
			&& `file --mime-type ${file}` != "${file}: text/plain" ]]; then
			git lfs track "$file"
		fi
	done
	git add -A .

	git commit -m "$MESSAGE" --quiet
	git push --quiet
else
	echo "Directory $MIRROR does not exist!"
fi
