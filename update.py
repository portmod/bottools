# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import argparse
import os
import sys

import gitlab
from importmod.update import get_updates


def get_project(group_name: str, project_name: str) -> gitlab.Gitlab:
    gl = gitlab.Gitlab.from_config()
    groups = gl.groups.list(name=group_name)
    projects: None
    if groups:
        for group in groups:
            if group.name.lower() == group_name.lower():
                projects = group.projects.list(search=project_name)
                break
    else:
        users = gl.users.list(search=group_name)
        for user in users:
            if user.name.lower() == group_name.lower():
                projects = user.projects.list(search=project_name)
                break
    if not projects:
        raise Exception(f"Cannot find user/group specified by {group_name}")

    glp = next(p for p in projects if p.path == project_name)
    return gl.projects.get(glp.id)


def gitlab_update():
    parser = argparse.ArgumentParser(
        description="Interface for checking for updates to existing mods and \
        creating issues automatically"
    )
    parser.add_argument("--period", help="period since last update")
    parser.add_argument(
        "--all",
        help="Checks for updates to all mods. \
        This is a slow operation and should be done infrequently",
        action="store_true",
    )
    parser.add_argument(
        "repository", help="path of the repository to check updates for"
    )
    parser.add_argument(
        "--gitlab-project",
        help="The gitlab project group/name for submitting the issues",
    )
    args = parser.parse_args()

    if len(sys.argv) == 1:
        parser.print_help()

    group_name, project_name = args.gitlab_project.split("/")
    proj = get_project(group_name, project_name)

    if not os.path.exists(os.path.join(args.repository, "profiles", "repo_name")):
        raise Exception(f"Repository does not exist at path {args.repository}")

    if args.period:
        updates = get_updates(period=args.period, repository=args.repository)
    elif args.all:
        updates = get_updates(repository=args.repository)

    for update in updates:
        # If issue already exists with atom and version in title, don't create issue.
        # This prevents duplicates and allows us to, even if we don't do an update,
        # close it and not be bothered again.
        # (E.g. Nexus mods has many mods with poorly formatted version numbers,
        # such as 1.52 instead of 1.5.2
        modname = update.oldatom.CPN
        if update.newatom:
            newver = update.newatom.PV
        else:
            newver = None
        # FIXME: Really we should only search issues tagged "update", but old issues may not be labeled as such
        possible = proj.issues.list(search=modname, all=True)
        possible_open = proj.issues.list(
            search=modname, all=True, labels=["update"], state="opened"
        )
        # FIXME: Will break if issue title template ever changes
        if not any(
            issue.title.strip() == f"[{modname}] Version {newver} is available"
            or issue.title.strip() == update.title.strip()
            for issue in possible
        ):
            new_issue = proj.issues.create(
                {
                    "title": update.title,
                    "description": update.description,
                    "labels": ["update"],
                }
            )

            # Close previous update issues for this package. Only if they are labeled as such.
            if newver:
                for issue in possible_open:
                    # Make sure it refers precisely to this package.
                    if "[" + modname + "]" in issue.title:
                        issue.discussions.create(
                            {
                                "body": f"A newer version is available. See #{new_issue.iid}"
                            }
                        )
                        issue.state_event = "close"
                        issue.save()

            print(update.title)
            print(update.description)
            print()


if __name__ == "__main__":
    gitlab_update()
