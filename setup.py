#!/usr/bin/env python

# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from setuptools import find_packages, setup

setup(
    name="bottools",
    author="Portmod Authors",
    description="A CLI tool for automatic interactions with Portmod's gitlab repos",
    license="GPLv3",
    url="https://gitlab.com/portmod/bottools",
    packages=find_packages(exclude=["*.test", "*.test.*", "test.*", "test"]),
    install_requires=["importmod>=0.4.0", "portmod>=2.0rc10", "python-gitlab"],
    setup_requires=["setuptools_scm"],
    use_scm_version=True,
)
